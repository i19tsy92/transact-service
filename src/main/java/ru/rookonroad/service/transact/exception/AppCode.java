package ru.rookonroad.service.transact.exception;

public enum AppCode {
    NOT_FOUND(404, "Объект не найден");


    private final Integer httpCode;
    private final String message;

    AppCode(Integer httpCode, String message) {
        this.httpCode = httpCode;
        this.message = message;
    }

    public Integer getHttpCode() {
        return this.httpCode;
    }

    public String getMessage() {
        return this.message;
    }
}
