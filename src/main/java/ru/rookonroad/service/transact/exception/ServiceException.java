package ru.rookonroad.service.transact.exception;

import lombok.Getter;

public class ServiceException extends RuntimeException{

    @Getter
    private String message;

    @Getter
    private AppCode code;

    public ServiceException(AppCode code) {
        if (code != null) {
            this.code = code;
            this.message = code.getMessage();
        }
    }

    public ServiceException(AppCode code, String message) {
        if (message != null) {
            this.message = message;
        } else {
            if (code != null) {
                this.message = code.getMessage();
            }
        }
        if (code != null) {
            this.code = code;
        }
    }

}
