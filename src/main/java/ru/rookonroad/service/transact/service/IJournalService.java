package ru.rookonroad.service.transact.service;

import ru.rookonroad.service.transact.model.Journal;

import java.util.List;

/**
 * Сервис доступа к журналу
 */
public interface IJournalService {

    /**
     * Найти все записи в журнале
     * @return список журналов
     */
    List<Journal> findAll();

    /**
     * Добавить журнал в БД
     * @param journal объект журнала
     * @return - сохраненный объект журнала
     */
    Journal add(Journal journal);
}
