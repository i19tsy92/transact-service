package ru.rookonroad.service.transact.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.transact.component.report.BaseReportBuilder;
import ru.rookonroad.service.transact.exception.ServiceException;
import ru.rookonroad.service.transact.model.Transaction;
import ru.rookonroad.service.transact.model.TransactionMessage;
import ru.rookonroad.service.transact.model.report.ReportType;
import ru.rookonroad.service.transact.service.IComparisonService;
import ru.rookonroad.service.transact.service.IReportService;
import ru.rookonroad.service.transact.service.ITransactionService;

@Service
@AllArgsConstructor
public class ComparisonService implements IComparisonService {

    private final IReportService reportService;
    private final ITransactionService transactionService;
    private final BaseReportBuilder reportBuilder;

    @Override
    public void compareAndReport(TransactionMessage message) {
        Transaction transaction;
        ReportType type;
        try {
            transaction = transactionService.getTransactionById(message.getId());
            if (transaction == null || !transaction.getAmount().equals(message.getAmount())) {
                type = ReportType.FAIL;
            } else {
                type = ReportType.SUCCESS;
            }
        } catch (ServiceException e) {
            type = ReportType.FAIL;
        }
        String reportMessage = "Transaction id: " + message.getId() + " with amount " + message.getAmount();
        reportService.report(reportBuilder.buildReport(reportMessage, type));
    }
}
