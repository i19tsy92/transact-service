package ru.rookonroad.service.transact.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.transact.component.report.BaseReporter;
import ru.rookonroad.service.transact.model.report.Report;
import ru.rookonroad.service.transact.service.IReportService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Lazy
@Slf4j
@Service
public class ReportService implements IReportService {

    private List<BaseReporter> reporters;

    @Override
    public void setReporters(List<BaseReporter> reporters) {
        this.reporters = reporters;
    }

    @Override
    public void report(Report report) {
        Map<String, Boolean> results = new HashMap<>();
        reporters.forEach(reporter -> {
            results.put(reporter.getName(), reporter.sendReport(report));
        });
        logResult(results);
    }

    private void logResult(Map<String, Boolean> results) {
        results.forEach((name, result) ->
                log.info("Отчет отправлен по каналу: " + name + ", результат: " + (result ? "Success" : "Fail")));
    }
}
