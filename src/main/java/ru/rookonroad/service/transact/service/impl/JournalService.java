package ru.rookonroad.service.transact.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.transact.model.Journal;
import ru.rookonroad.service.transact.repository.JournalRepository;
import ru.rookonroad.service.transact.service.IJournalService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class JournalService implements IJournalService {

    private final JournalRepository journalRepository;

    @Override
    public List<Journal> findAll() {
        return journalRepository.findAll();
    }

    @Override
    public Journal add(Journal journal) {
        journal.setId(UUID.randomUUID());
        journal.setDatetime(LocalDateTime.now());
        return journalRepository.save(journal);
    }
}
