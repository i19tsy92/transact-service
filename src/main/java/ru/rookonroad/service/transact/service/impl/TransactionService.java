package ru.rookonroad.service.transact.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFutureCallback;
import ru.rookonroad.service.transact.exception.AppCode;
import ru.rookonroad.service.transact.exception.ServiceException;
import ru.rookonroad.service.transact.model.KafkaMessage;
import ru.rookonroad.service.transact.model.Transaction;
import ru.rookonroad.service.transact.repository.TransactionRepository;
import ru.rookonroad.service.transact.service.ITransactionService;

@Slf4j
@Service
@AllArgsConstructor
public class TransactionService implements ITransactionService {

    private final TransactionRepository repository;
    private final KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public Transaction getTransactionById(Integer id) {
        return repository.findById(id).orElseThrow(() -> new ServiceException(AppCode.NOT_FOUND));
    }

    @Override
    public void sendTransactionToTopic(KafkaMessage transaction) {
        kafkaTemplate.send("send-transaction", transaction.toString())
                .addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
                    @Override
                    public void onFailure(Throwable ex) {
                        log.error("Не получилось отправить транзакцию: " + transaction.toString()
                                + " в топик send-transaction ");
                    }

                    @Override
                    public void onSuccess(SendResult<String, String> result) {
                        log.info(transaction.toString() + " успешно отправлена в топик send-transaction");
                    }
                });
    }

}
