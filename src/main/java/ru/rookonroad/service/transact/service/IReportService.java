package ru.rookonroad.service.transact.service;

import ru.rookonroad.service.transact.component.report.BaseReporter;
import ru.rookonroad.service.transact.model.report.Report;
import ru.rookonroad.service.transact.model.report.ReportJsonMessage;

import java.util.List;

/**
 * Сервис отправки ответов
 */
public interface IReportService {

    /**
     * Сервисный метод для установки списка доступных репортеров
     * @param reporters - список репортеров
     */
    void setReporters(List<BaseReporter> reporters);

    /**
     * Отправка репорта посредством всех доступных репортеров
     * @param report - отчет
     */
    void report(Report report);
}
