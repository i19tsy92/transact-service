package ru.rookonroad.service.transact.service;

import ru.rookonroad.service.transact.model.TransactionMessage;

/**
 * Сервис сравнения
 */
public interface IComparisonService {

    /**
     * Сравнить транзакции и отправить отчет
     * @param message - сообщение транзакции
     */
    void compareAndReport(TransactionMessage message);
}
