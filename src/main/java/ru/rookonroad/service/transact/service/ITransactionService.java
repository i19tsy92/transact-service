package ru.rookonroad.service.transact.service;

import ru.rookonroad.service.transact.model.KafkaMessage;
import ru.rookonroad.service.transact.model.Transaction;

/**
 * Сервис доступа к транзакциям
 */
public interface ITransactionService {

    /**
     * Получение транзакции по идентификатору
     * @param id - идентификатор транзакции
     * @return - транзакцию по идентификатору
     */
    Transaction getTransactionById(Integer id);

    /**
     * Отправка транзакции в топик кафки
     * @param transaction сообщение транзакции для кафки
     */
    void sendTransactionToTopic(KafkaMessage transaction);
}
