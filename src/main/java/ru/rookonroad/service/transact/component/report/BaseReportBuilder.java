package ru.rookonroad.service.transact.component.report;

import ru.rookonroad.service.transact.model.TransactionMessage;
import ru.rookonroad.service.transact.model.report.Report;
import ru.rookonroad.service.transact.model.report.ReportType;

/**
 * Строитель для отчетов
 */
public interface BaseReportBuilder {

    /**
     * Создать отчет
     * @param message сообщение отчета
     * @param type тип отчета
     * @return готовый отчет
     */
    Report buildReport(String message, ReportType type);
}
