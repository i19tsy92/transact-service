package ru.rookonroad.service.transact.component;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.rookonroad.service.transact.component.report.BaseReporter;
import ru.rookonroad.service.transact.service.IReportService;

import java.util.ArrayList;
import java.util.List;


/**
 * ПЗаполнение списка репортеров для сервиса отчетов
 */
@Slf4j
@Component
@AllArgsConstructor
public class ReportServiceConfigurationApplicationRunner implements ApplicationRunner {

    private final ApplicationContext context;
    @Lazy
    @Autowired
    private IReportService reportService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<BaseReporter> reporters = new ArrayList<>();
        context.getBeansOfType(BaseReporter.class).forEach((name, reporter) -> {
            log.info("Load reporter: " + name + "...");
            reporters.add(reporter);
            log.info("Reporter: " + reporter.getName() + " injected");
        });
        reportService.setReporters(reporters);
    }
}
