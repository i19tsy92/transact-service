package ru.rookonroad.service.transact.component.consumer;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.rookonroad.service.transact.model.TransactionMessage;
import ru.rookonroad.service.transact.service.IComparisonService;
import ru.rookonroad.service.transact.util.Convertor;

import java.math.BigDecimal;

/**
 * Получатель сообщений о транзакциях из кафки
 */
@Slf4j
@Component
public class KafkaConsumer extends BaseConsumer<String>{

    public KafkaConsumer(IComparisonService comparisonService) {
        super(comparisonService);
    }

    @KafkaListener(topics = "send-transaction", groupId = "transaction_group_id")
    public void consume(String message) {
        log.info(message + " consumed");
        super.consumeMessage(message);
    }

    @Override
    public TransactionMessage convertToTransactionMessage(String message) {
        TransactionMessage transactionMessage = new TransactionMessage();
        JSONParser parser = new JSONParser();
        try {
            JSONObject object = (JSONObject) parser.parse(message);
            log.info("Input object: " + object.toJSONString());
            if (object.get("transactionId") != null) {
                transactionMessage.setId(Convertor.toInteger(object.get("transactionId").toString()));
            }
            if (object.get("amount") != null) {
                transactionMessage.setAmount(new BigDecimal(object.get("amount").toString()));
            } else {
                transactionMessage.setAmount(new BigDecimal("0.0"));
            }
            if (object.get("data") != null) {
                transactionMessage.setData(object.get("data").toString());
            } else {
                transactionMessage.setData("");
            }
        } catch (ParseException e) {
            log.error(e.toString() + " " + e.getMessage() + " - error while parsing message");
        }
        return transactionMessage;
    }
}
