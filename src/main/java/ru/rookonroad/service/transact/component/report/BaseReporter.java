package ru.rookonroad.service.transact.component.report;

import ru.rookonroad.service.transact.model.report.Report;

/**
 * Базовый класс для отправки репортов
 */
public interface BaseReporter {

    /**
     * Отправить репорт
     * @param report - репорт
     * @return результат отправки
     */
    Boolean sendReport(Report report);

    /**
     * Получить имя репортера
     * @return - имя репортера
     */
    String getName();
}
