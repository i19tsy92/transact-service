package ru.rookonroad.service.transact.component.report;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;
import ru.rookonroad.service.transact.model.report.Report;

@Slf4j
@Component
@AllArgsConstructor
public class KafkaReporter implements BaseReporter{

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public Boolean sendReport(Report report) {
        kafkaTemplate.send("reports", report.getMessage()).addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error("Report not delivered. Reason: " + ex.getMessage());
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                log.info("Report delivered. " + result.toString());
            }
        });
        return true;
    }

    @Override
    public String getName() {
        return "KAFKA";
    }
}
