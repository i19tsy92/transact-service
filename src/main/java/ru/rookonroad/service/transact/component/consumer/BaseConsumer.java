package ru.rookonroad.service.transact.component.consumer;

import lombok.extern.slf4j.Slf4j;
import ru.rookonroad.service.transact.model.TransactionMessage;
import ru.rookonroad.service.transact.service.IComparisonService;

/**
 * Базовый класс для создания получателей транзакционных сообщений
 * @param <M> - тип сообщения
 */
@Slf4j
public abstract class BaseConsumer<M> {

    protected IComparisonService comparisonService;

    protected BaseConsumer(IComparisonService comparisonService) {
        this.comparisonService = comparisonService;
    }

    protected void consumeMessage(M message) {
        comparisonService.compareAndReport(convertToTransactionMessage(message));
    }

    protected abstract void consume(M message);
    protected abstract TransactionMessage convertToTransactionMessage(M message);
}
