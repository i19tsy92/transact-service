package ru.rookonroad.service.transact.component.consumer;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.rookonroad.service.transact.model.Journal;
import ru.rookonroad.service.transact.service.IJournalService;
import ru.rookonroad.service.transact.util.Convertor;


/**
 * Кафка получатель для хурналов
 */
@Slf4j
@Component
@AllArgsConstructor
public class JournalConsumer {

    private final IJournalService journalService;

    @KafkaListener(topics = "reports", groupId = "transaction_group_id")
    protected void consume(String message) {
        log.info("Consumed message: " + message);
        journalService.add(convertToTransactionMessage(message));
    }

    protected Journal convertToTransactionMessage(String message) {
        Journal journal = new Journal();
        JSONParser parser = new JSONParser();
        try {
            JSONObject object = (JSONObject) parser.parse(message);
            log.info("Input object: " + object.toJSONString());
            if (object.get("message") != null) {
                journal.setMessage(object.get("message").toString());
            }
            if(object.get("confirmed") != null) {
                journal.setConfirmed(Convertor.toInteger(object.get("confirmed").toString()));
            }
        } catch (ParseException e) {
            log.error(e.toString() + " " + e.getMessage() + " - error while parsing message");
        }
        return journal;
    }
}
