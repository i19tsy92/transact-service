package ru.rookonroad.service.transact.component.report;

import ru.rookonroad.service.transact.model.report.Report;
import ru.rookonroad.service.transact.model.report.ReportJsonMessage;
import ru.rookonroad.service.transact.model.report.ReportType;

import java.time.LocalDateTime;

public class JsonStringReportBuilder implements BaseReportBuilder {
    @Override
    public Report buildReport(String message, ReportType type) {
        return new ReportJsonMessage(message, type == ReportType.SUCCESS ? 1 : 0, LocalDateTime.now());
    }
}
