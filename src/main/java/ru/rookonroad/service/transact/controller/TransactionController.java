package ru.rookonroad.service.transact.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rookonroad.service.transact.model.KafkaMessage;
import ru.rookonroad.service.transact.service.ITransactionService;

/**
 * Контроллер для отправки транзакций в очередь
 */
@RestController
@RequestMapping("/transaction")
@AllArgsConstructor
public class TransactionController {

    private final ITransactionService transactionService;

    @PostMapping("/send")
    public ResponseEntity<Boolean> sendTransaction(@RequestBody KafkaMessage transaction) {
        transactionService.sendTransactionToTopic(transaction);
        return ResponseEntity.ok(true);
    }
}
