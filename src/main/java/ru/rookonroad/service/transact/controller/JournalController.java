package ru.rookonroad.service.transact.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rookonroad.service.transact.model.Journal;
import ru.rookonroad.service.transact.service.IJournalService;

import java.util.List;

/**
 * Контролелер для доступа к сообщениям журнала
 */
@RestController
@RequestMapping("/journal")
@AllArgsConstructor
public class JournalController {

    private final IJournalService journalService;

    @GetMapping
    public ResponseEntity<List<Journal>> findAllJournals() {
        return ResponseEntity.ok(journalService.findAll());
    }
}
