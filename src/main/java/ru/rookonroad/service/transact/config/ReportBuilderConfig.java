package ru.rookonroad.service.transact.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rookonroad.service.transact.component.report.BaseReportBuilder;
import ru.rookonroad.service.transact.component.report.JsonStringReportBuilder;

@Configuration
public class ReportBuilderConfig {


    @Bean
    public BaseReportBuilder reportBuilder() {
        return new JsonStringReportBuilder();
    }
}
