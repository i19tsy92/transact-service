package ru.rookonroad.service.transact.util;


import org.apache.commons.lang3.StringUtils;


/**
 * Конвертор
 */
public final class Convertor {

    /**
     * Конвертировать в лонг
     * @param input
     * @return
     */
    public static Long toLong(String input) {
        if (StringUtils.isBlank(input)) {
            return null;
        }
        try {
            return Long.parseLong(input);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Конвертировать в инт
     * @param input
     * @return
     */
    public static Integer toInteger(String input) {
        if (StringUtils.isBlank(input)) {
            return null;
        }
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
