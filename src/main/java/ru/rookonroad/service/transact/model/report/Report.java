package ru.rookonroad.service.transact.model.report;

/**
 * Базовый класс отчета
 */
public interface Report {

    /**
     * ФОрматирвоание и получение сообщения
     * @return - строка отчета
     */
    String getMessage();
}
