package ru.rookonroad.service.transact.model.report;

/**
 * Тип отчета
 */
public enum ReportType {
    SUCCESS,
    FAIL
}
