package ru.rookonroad.service.transact.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Сообщение транзакции для отправки в очередь кафки
 */
@Getter
@Setter
@NoArgsConstructor
public class KafkaMessage implements Serializable {
    private static final long serialVersionUID = 3261163112468584686L;

    private Integer transactionId;
    private Double amount;
    private String data;

    @Override
    public String toString() {
        return "{" +
                "\"transactionId\":" + transactionId +
                ", \"amount\":" + amount +
                ", \"data\":" + "\"" + data + "\"" +
                "}";
    }
}
