package ru.rookonroad.service.transact.model;

import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Сущность транзакции
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@TypeDef(
        name = "json",
        typeClass = JsonType.class
)
@Table(name = "transactions")
public class Transaction implements Serializable {

    @Id
    private Integer id;

    private BigDecimal amount;

    @Type(type = "json")
    @Column(columnDefinition = "jsonb")
    private String data;

    @Override
    public String toString() {
        return "{" +
                "transactionId:" + id +
                ", amount:" + amount +
                ", data:'" + data + '\'' +
                '}';
    }
}
