package ru.rookonroad.service.transact.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * Сообщение транзакции полученное из кафки
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class TransactionMessage implements Serializable {

    private static final long serialVersionUID = -6971388789514998242L;

    private Integer id;
    private BigDecimal amount;
    private String data;
}
