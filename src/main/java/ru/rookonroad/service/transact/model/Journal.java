package ru.rookonroad.service.transact.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "journal")
public class Journal {

    @Id
    private UUID id;

    private String message;

    private Integer confirmed;

    private LocalDateTime datetime;
}
