package ru.rookonroad.service.transact.model.report;

import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * JSON сообщение отчета
 */
@Setter
@NoArgsConstructor
public class ReportJsonMessage implements Serializable, Report {

    private static final long serialVersionUID = -5288048183608792056L;

    private String message;
    /**
     * Транзакция подтверждена confirmed = 1
     * Транзакция отклонена confirmed = 0
     */
    private Integer confirmed;
    private LocalDateTime timestamp;

    public ReportJsonMessage(String message, Integer confirmed, LocalDateTime timestamp) {
        this.message = message;
        this.confirmed = confirmed;
        this.timestamp = timestamp;
    }

    @Override
    public String getMessage() {
        return "{" + "\"message\"" + ":" + "\"" + message + "\""  +
                "," + "\"confirmed\"" + ":" + confirmed + "," +
                "\"timestamp\"" + ":" + "\"" + timestamp.toString() + "\"" + "}";
    }
}
