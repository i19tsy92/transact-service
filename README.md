# Для запуска приложения необходимо: 
   * Выполнить mvn clean install -P dev в директории проекта
   * Выполнить docker compose up -d 
   * transact-service не запустится 
   * Выполнить в директории проекта mvn liquibase:update -Dprofile=dev
   * Выполнить docker compose up transact-service -d 
   
# Для проверки работы сервиса необходимо:
    * Выполнить запрос POST http://{host}:{port}/transaction/send
    Тело запроса: { "transactionId": 465, "amount": 101.05, "data": "test" }
    * Выполнить запрос GET http://{host}:{port}/journal
    * В ответе будет получен список объектов журнала, в котором можно будет найти результат проверки транзакции
    * По умолчанию host = localhost, port = 8080
