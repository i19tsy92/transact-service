FROM amazoncorretto:11
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
COPY transact-serivce/.env .env
ENTRYPOINT ["java", "-DenvFile=.env", "-jar", "/app.jar"]
